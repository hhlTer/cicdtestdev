package com.qa.cicdtest;

import controllers.MainPage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"controllers"})
public class CicdtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CicdtestApplication.class, args);
	}

}
